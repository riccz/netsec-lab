\documentclass[a4paper,11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage[margin=2.54cm]{geometry}

\usepackage{codecmds}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

\usepackage{tikz}
\usetikzlibrary{shapes,chains,scopes,arrows.meta}

%\usepackage{import}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage{color}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage[section]{placeins}

%\usepackage[backend=biber]{biblatex}
%\addbibresource{}

\author{Riccardo Zanol}
\date{May 8, 2017}
\title{Wireless Network Security \\ Lab. 1}

\begin{document}
\matlabcodeconfig
\maketitle

\section{Tutorial on Certification Authorities}
The instructions given in the first part build a simple test PKI with
a root CA and two intermediate CAs that issue certificates to two
clients.
%
The final situation is represented in Figure~\ref{fig:setup}, where
the arrows indicate which entity issued a certificate. Using this PKI,
if both clients trust the root CA, then they can trust the public key
of the other client to actually belong to it by following the chain of
certificates.
%
\begin{figure}[h]
  \centering
  \begin{tikzpicture}
    \begin{scope}[every node/.style={ellipse,thick,draw}]
      \node (rootca) at (0,0) {Root CA};
      \node (caa) at (-1.5,-1.5) {CA A};
      \node (cab) at (1.5,-1.5) {CA B};
      \node (client1) at (-1.5, -3) {Client 1};
      \node (client2) at (1.5, -3) {Client 2};
    \end{scope}

    \begin{scope}[>={Stealth[black]},
      every edge/.style={draw,thick}]
      \path [->] (rootca) edge (caa);
      \path [->] (rootca) edge (cab);
      \path [->] (caa) edge (client1);
      \path [->] (cab) edge (client2);
    \end{scope}
  \end{tikzpicture}
  \caption{Test PKI setup. The arrows indicate a certificate
    issuance.}
  \label{fig:setup}
\end{figure}

Using the OpenSSL tools to examine the built certificates it is
possible to see that, as expected, they include the public key $(n,d)$
of the subject, a subject identifier, a signature from an issuer and
the issuer identifier. The identifiers are of the form \texttt{C=IT,
  ST=Padova, L=Padova, O=UniPD, OU=Lecture on Network Security, CN=RZ
  ca-a/emailAddress=ricc@zanol.eu}.

The certificate requests, instead, just contain the identifier of the
requesting entity, its public key and a signature from the requester's
private key.

There are some differences between the five certificates: the two
client certificates include the allowed uses for the keys
(client/server authentication) and a flag that prevents them from
issuing other certificates. The three CA certificates, instead, have
the CA flag set to true, so the certificates signed by them are
accepted as valid (provided that the CA certificate is trusted or it
is signed by a trusted entity).
%
The root certificate has the same structure of the intermediate CA
certificates but it is self-signed: the issuer and the subject have
the same identifier and the signature is done with the private key
corresponding to the public key in the certificate.

Examining the private key files it is possible to notice some
differences in the OpenSSL implementation of RSA with respect to the
algorithm explained in the lectures.
%
Instead of picking a private exponent $d$ coprime with $\phi =
(p-1)(q-1)$ and computing the public exponent $e$ such that $ed=1 \mod
\phi$, OpenSSL fixes the public exponent $e = 65537$, checks that it
is coprime with $\phi$ (and picks two different primes $p$ and $q$ in
that case) and then computes a private exponent $d$ such that $ed=1
\mod \phi$.
%
It also stores three additional parameters: ``exponent1'',
``exponent2'' and ``coefficient'' that are derived from $p$, $q$ and
$d$ and are used to improve the performance of the decryption
operation by exploiting the Chinese remainder theorem.

The lengths of the parameters are consistent with the specified
command-line arguments: the two primes $p$ and $q$ are 1024-bits long
and the modulus $n=pq$ is 2048-bits long. All three of them (and some
of the other parameters) have an additional leading byte of value zero
which is needed because they are assumed to be signed integers.

\section{SSL/TLS based network services}
The second part uses the PKI that was built in the first part to test
the TLS protocol using two tools included in OpenSSL and another tool
used to protect generic TCP connections with TLS.

\subsection{s\_client and s\_server}
This two tools are tested on the same host using the certificate and
keys for Client 1 as the server entity, while the client authenticates
itself as Client 2.
%
By observing the packets exchanged by s\_client and
s\_server it is possible to see that they follow the TLS
handshake protocol explained in the lectures.

The difference between one-way authentication and mutual
authentication can be seen in the packet captures: in the first case
the client does not send its certificate to the server in the third
step of the handshake, while in the second case it can be seen that
both the server and the client send their certificates to each other.
%
To be able to verify the chain of certificates, the client needs to
have the certificate of CA A, which signed the server certificate, in
addition to the certificates for the server itself and the trusted
root.  To solve this issue the server must be configured to send the
certificate for its intermediate CA together with its own in the
second step of the handshake.
%
The same problem arises in the case of mutual authentication for the
certificate of the CA that signed the client's certificate.
%
If, for example, the client does not send the certificate for CA B,
the server is unable to authenticate it and writes out an error:
\texttt{unable to get local issuer certificate}. With the settings
used it still allows the handshake with the unauthenticated client to
complete, but a real server would stop the TLS session.

Since the s\_server and s\_client tools do not allow to specify
separately the trusted root certificates and the set of intermediate
certificates to be sent to the other end, in the case of mutual
authentication both the root and either CA A or CA B are trusted and
sent during the handshake. This is redundant because the root CA is
known to everyone and the intermediate CA would be trusted anyway
because its certificate is issued by the root CA.

In the packet captures it can be seen that both the client and the
server set the PSH flag on all the packets they send and reply with
empty ACKs, so the number of packets required to complete the
handshake is 8 instead of the 4 (plus the final ACK) that would be
needed if the ACKs were piggybacked.
%
This also happens when data packets are sent, but in this case the
ACKs would be empty anyway since the server just receives the text
typed into the client and doesn't reply.

If s\_server is run without additional options, it warns about the
usage of a default set of Diffie-Hellman parameters. The public
parameters are an integer $n$, which implies the choice of the finite
group $\mathbb{G} = \mathbb{Z}/n$, and $\alpha$ a primitive element of
$\mathbb{G}$.
%
The command \texttt{openssl dhparam -5 > dhparfile} can be used to
generate a new set of DH parameters with $\alpha = 5$ and an $n$ of
the desired length (e.g. 2048 bits) such that $\alpha$ is a primitive
element for the resulting group.
%
This parameters are then sent, together with an ephemeral DH public
key, to the client during the second step of the TLS handshake. This
means that the client has no choice on which DH parameters to use, it
can only decide whether to perform the key exchange with
Diffie-Hellman or a different algorithm (e.g. encrypting a random
pre-master secret with the server's public RSA key) at the start of
the handshake.

Both the client and the server can restrict their supported cipher
list: for example to only use ephemeral DH key exchange one or both of
them can set the option \texttt{-cipher 'kEDH'} or they can select a
specific set of security mechanisms, like \texttt{-cipher
  'DHE-RSA-AES256-GCM-SHA384'}. The use of the ephemeral DH keys
prevents a disclosure of the server certificate's private-key to allow
the decryption of past TLS sessions: in this case the master secret is
derived from the DH shared secret key, which is generated during the
handshake and not reused.

During the handshake the intersection of the mechanisms supported by
the client and the server is selected.
%
It is also possible to not use any encryption mechanism (with
\texttt{eNULL} on both the client and the server), in this case the
clear-text shows up in the packet captures.

Among the key exchange mechanisms there is also the elliptic-curve
version of DH. For this algorithm, instead of generating a set of
parameters, a particular elliptic curve must be picked among the ones
supported by OpenSSL.

\subsection{stunnel}
The given configuration for the stunnel client and server sets them up
in the way shown in Figure~\ref{fig:stunnel}: the telnet client,
running on PC-B connects to the telnet server on PC-A through a TLS
tunnel created by stunnel.  In this configuration TLS performs only
one-way authentication: the client does not use a certificate to
authenticate itself to the server.

When the two stunnel instances start up, there is no exchange of
packets between them until a connection is attempted on the listening
port at the client side (PC-B:3200).
%
After the telnet client tries to open a connection, the stunnel client
start a TLS handshake with the stunnel server listening on the server
side (PC-A:1500).
%
As soon as the TLS session is set up, the stunnel server opens a
connection to port 23 on the same host (PC-A:23) and, from now on, the
data that is sent to port 2300 on the client is forwarded to port 23
on the server and vice-versa, thus allowing the telnet client to
connect to the server using a secret, authenticated and
integrity-protected channel.
%
\begin{figure}[h]
  \centering
  \begin{tikzpicture}[block/.style={rectangle,thick,draw},
      tunnel/.style={cylinder,draw,thick,aspect=0.3,shape border rotate=0,
        minimum height=7cm,minimum width=1cm},
      >={Stealth[black]},
      edge/.style={draw,thick}]

    \node[tunnel] (tunnel) at (0,0) {
      \begin{tikzpicture}[every node/.style={minimum height=0, minimum width=0}]
        \node[block] (stunnelsrv) at (-3cm,0) {Stunnel server};
        \node[block] (stunnelclient) at (3cm,0) {Stunnel client};
      \end{tikzpicture}
    };

    \node[block] (telnetsrv) at (-6cm, 1) {Telnet server};
    \node[block] (telnetclient) at (6cm, 1) {Telnet client};

    \path[edge] [<->] (telnetsrv) |- (tunnel)
    node[left,near start] {PC-A:23};
    \path[edge] [<->] (stunnelsrv) edge (stunnelclient)
    node[above left= 0cm and -0.6cm,near start] {PC-A:1500};
    \path[edge] [<->] (telnetclient) |- (tunnel)
    node[near end, below right= 0cm and -0.6cm] {PC-B:2300};
  \end{tikzpicture}
  \caption{TLS tunnel setup}
  \label{fig:stunnel}
\end{figure}

If the telnet client, instead, connects directly to the server at port
23 it is possible to observe the commands sent in clear-text in the
packet capture.
\end{document}
