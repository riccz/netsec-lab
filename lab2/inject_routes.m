function [T] = inject_routes(G, T_orig, F, D)
assert(D ~= F);
T = T_orig;

T(F,D,:) = [D, 0, 1];
m = 1;
last_updated = [F];
while ~isempty(last_updated)
    t = [];
    for i = 1:length(last_updated)
        v = last_updated(i);
        to_update = find(G(:,v));
        for j = 1:length(to_update)
            u = to_update(j);
            if T(u, D, 2) > m
                T(u, D, :) = [v, m, 1];
                t = [t, u];
            end
        end
    end
    m = m + 1;
    last_updated = t;
end
end
