close all; clear all; clc;
delete('matlab.log');
diary('matlab.log');

rng('shuffle');

N = 50; L = 10; R = 3;
iter = 500;
methods = {'random', '2hops', 'maxdist'};

for m = 1:length(methods)
    CR_vec = nan(iter, 1);
    CRnew_vec = nan(iter, 1);
    parfor i = 1:iter
        np = setup_network(N,L,R);
        F = choose_F(N, np.x);
        [CR_vec(i), ~] = count_routes(np.T, F);
        
        D = choose_D(np.x, np.T, F, methods{m}, []);
        
        Tnew = inject_routes(np.G, np.T, F, D);
        [CRnew_vec(i), ~] = count_routes(Tnew, F);
    end
    
    fprintf('Selection method %s:\n', methods{m});
    fprintf('Avg. controlled routes, honest F: %f\n', mean(CR_vec));
    fprintf('Avg. controlled routes, one fake dest: %f\n', mean(CRnew_vec));
    fprintf('Avg. diff of controlled routes, one fake dest: %f\n', mean(CRnew_vec - CR_vec));
    fprintf('\n');
end

diary('off');
