close all; clear all; clc;

N = 50; L = 10; R = 3;
np = setup_network(N,L,R);

F = choose_F(N, np.x);

[CR, C] = count_routes(np.T, F);
pC = color_routes(np.x,np.T,C,F);

print('true_routes.eps', '-depsc');

pC_del = pC(~isnan(pC));
if ~isempty(pC_del)
    delete(pC_del);
end
clear pC_del;

fprintf(1,'Attacker node %d controls %d out of %d routes:\n',F,CR,(N-1)^2)
controlled_fraction = CR / (N-1)^2;

figure(2), imagesc(C)
print('true_mask.eps', '-depsc');

figure(1);
if CR > 0,
    [Slist,Dlist] = find(C);
    fprintf(1,'route from %d to %d\n',[Slist';Dlist'])
end

D = choose_D(np.x, np.T, F, 'maxdist', []);
Tnew = inject_routes(np.G, np.T, F, D);


[CRnew,Cnew] = count_routes(Tnew,F);
[Slist, Dlist] = find(Cnew);
fprintf(1,"Attacker node %d controls %d out of %d routes:\n",F,CRnew,(N-1)^2)
fprintf(1,"route from %d to %d\n",[Slist';Dlist'])
fprintf("\n");

fprintf("The attacker is now in %d more routes\n", CRnew - CR);

pC = color_routes(np.x,Tnew,Cnew,F);

print('fake_routes.eps', '-depsc');

pC_del = pC(~isnan(pC));
if ~isempty(pC_del)
    delete(pC_del);
end
clear pC_del;

figure(2), imagesc(Cnew)

print('fake_mask.eps', '-depsc');
