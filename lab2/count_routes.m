function [CR, C] = count_routes(T, F)
C = (T(:,:,1) == F);
C(:,F) = 0; C(F,:) = 0;
Dlist = find(sum(C,1));
for D = Dlist(:)'
    Slist = find(C(:,D));
    while ~isempty(Slist)
        R = Slist(1);
        Snew = find(T(:,D,1) == R);
        C(Snew,D) = 1;
        Slist = [Slist(2:end); Snew];
    end
end
CR = sum(sum(C));
end
