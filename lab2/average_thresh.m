close all; clear all; clc;
delete('matlab.log');
diary('matlab.log');

rng('shuffle');

N = 50; L = 10; R = 3;
iter = 500;
thresholds = [0.2, 0.5, 0.8];

for t = 1:length(thresholds)
    needed_D = nan(iter, 1);
    parfor i = 1:iter
        np = setup_network(N,L,R);
        F = choose_F(N, np.x);
        [CR, ~] = count_routes(np.T, F);
        Ds = [];
        T = np.T;
        succ = true;
        while CR < thresholds(t) * (N-1)^2
            if length(Ds) >= N-1
                succ = false;
                break;
            end
            D = choose_D(np.x, T, F, 'maxdist', Ds);
            Ds = [Ds D];
            
            T = inject_routes(np.G, T, F, D);
            [CR, ~] = count_routes(T, F);
        end
        if succ
            needed_D(i) = length(Ds);
        end
    end
    
    fprintf('Threshold %f:\n', thresholds(t));
    fprintf('Can reach the threshold wp %f\n', sum(~isnan(needed_D))/iter);
    fprintf('Avg. needed nodes: %f\n', mean(needed_D(~isnan(needed_D))));
    fprintf('\n');
end

diary('off');
