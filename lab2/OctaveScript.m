clear all
page_output_immediately
more off
figure(1)
N = 50; L =10; R = 3; density = N/L^2*pi*R^2
x = L*rand(2,N);
clf, hold on
pV = plot(x(1,:),x(2,:),'.');
text(x(1,:),x(2,:),int2str((1:N)'),'FontSize',16);
G = zeros(N,N); pG = nan(N,N);
disp('Building network graph...')
for i = 1:N;
    for j = i+1:N;
        y = [];
        if norm(x(:,i)-x(:,j)) < R,
            G(i,j) = 1; G(j,i) = 1;
            pG(i,j) = plot(x(1,[i,j]),x(2,[i,j]),'b-'); pause(0.02);
            axis equal
        end
    end
end
degree = sum(sum(G))/N
edge_fraction = degree/(N-1)

conn = yes_or_no('Look at the graph representing the network. Is it connected? ');
if ~conn
    disp('Network not connected. Simulation aborted. Re-run a new one.')
    return
end

T = nan(N,N,3); 
T(:,:,2) = inf; 
T(:,:,3) = 0;
pT = nan(N,N);
disp('Building routing tables...')
for D = 1:N;
    disp(['for destinaton D = ', int2str(D)]);
    m = 0;
    T(D,D,:) = [D,m,1];
    pT(D,D) = plot(x(1,D),x(2,D),'go','MarkerSize',20,'LineWidth',5);
    Rlist = [D];
    while any(isnan(T(:,D,1)))
        Slist = []; m = m+1;
        for R = Rlist';
            for S = find(G(R,:));
                if T(S,D,2) > m;
                    Slist = [Slist; S];
                    T(S,D,:) = [R,m,1];
                    pT(S,R) = plot(x(1,[S,R]),x(2,[S,R]),'g-','LineWidth',4);
                end
            end
        end
        Rlist = Slist;
    end
    drawnow;
    pause(0.02)
    delete(pT(~isnan(pT))); pT = nan(N,N);
end
diameter = max(max(T(:,:,2)))
T(:,:,1)

F = randi(N);
fprintf('Attacker is node %d\n',F)
plot(x(1,F),x(2,F),'ro','MarkerSize',20,'LineWidth',5)

C = (T(:,:,1) == F);
C(:,F) = 0; C(F,:) = 0;
Dlist = find(sum(C,1));
pTF = nan(N,N);
for D = Dlist(:)'
    Slist = find(C(:,D));
    for S = Slist(:)'
        if isnan(pTF(S,F))
            pTF(S,F) = plot(x(1,[S,F]),x(2,[S,F]),'r-','LineWidth',4);
        end
    end
    while ~isempty(Slist)
        R = Slist(1);
        Snew = find(T(:,D,1) == R);
        for S = Snew(:)'
            if isnan(pTF(S,R))
                pTF(S,R) = plot(x(1,[S,R]),x(2,[S,R]),'r-','LineWidth',4);
            end
        end
        drawnow
        pause(0.01)
        C(Snew,D) = 1;
        Slist = [Slist(2:end); Snew];
    end
end
CR = sum(sum(C));
fprintf(1,'Attacker node %d controls %d out of %d routes:\n',F,CR,(N-1)^2)
figure(2), imagesc(C)
if CR > 0,
    [Slist,Dlist] = find(C);
    fprintf(1,'route from %d to %d\n',[Slist';Dlist'])
end
