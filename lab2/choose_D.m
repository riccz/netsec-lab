function D = choose_D(x, T, F, method, exclude)
N = size(T,1);
if all(exclude ~= F)
    exclude = [exclude F];
end
D = F;
while any(exclude == D)
    if strcmp(method, 'random')
        D = randperm(N, 1);
    elseif strcmp(method, '2hops')
        F_twohops = find(T(F,:, 2) == 2);
        D = F_twohops(randi(length(F_twohops)));
    elseif strcmp(method, 'maxdist')
        allowed_nodes = setdiff(1:N, exclude);
        maxhops = max(T(F,allowed_nodes,2));
        F_maxhops = find(T(F,:,2) == maxhops);
        D = F_maxhops(randi(length(F_maxhops)));
    else
        error('Give a selection method');
    end
end
fprintf("Victim is node %d\n",D);
plot(x(1,D),x(2,D),'og', 'MarkerSize', 20);

end
