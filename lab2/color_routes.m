function pC = color_routes(x, T, C, F)
N = size(x,2);
pC = nan(N,N);
[Slist, Dlist] = find(C);
for i = 1:length(Slist)
    u = Slist(i); v = Dlist(i);
    while u ~= v && u ~= F
        w = T(u, v, 1);
        if isnan(pC(u,w)) && isnan(pC(w,u))
            pC(u,w) = plot(x(1,[u,w]),x(2,[u,w]),'r-','LineWidth',4);
        end
        u = w;
    end
end
end
