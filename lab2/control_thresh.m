controlled_thresh = 0.8;
N = 50; L =10; R = 3; dV = N/L^2 * pi*R^2;
x = L*rand(2,N);

clf;
hold on;

plot(x(1,:),x(2,:),'.b', 'MarkerSize', 12);
text(x(1,:),x(2,:),int2str((1:N)'));

G = zeros(N,N);
%pG = nan(N,N);
disp("Building network graph...");
for i = 1:N
    for j = i+1:N
        if norm(x(:,i)-x(:,j)) < R
            G(i,j) = 1; G(j,i) = 1;
            %pG(i,j) = ...
            plot(x(1,[i,j]),x(2,[i,j]),'b-');
            %pG(j,i) = pG(i,j);
            %pause(0.02);
        end
    end
end

degG = sum(sum(G))/N;
wG = degG/(N-1);

fprintf("Nodes per coverage area: %f\n", dV);
fprintf("Average degree: %f\n", degG);
fprintf("Fraction of active links: %f\n", wG);

%conn = yes_or_no("Look at the graph representing the network. Is the graph connected? ");
%if ~conn, disp("Network not connected. Simulation aborted. Re-run a new one."), return, end

T = nan(N,N,3);
pl = nan(N,1);
T(:,:,2) = inf;
T(:,:,3) = 0;

disp("Building routing tables...")
for D = 1:N
    %disp(["for destinaton D = ", int2str(D)])
    m = 0;
    T(D,D,:) = [D,m,1];
    Rlist = [D];
    while ~isempty(Rlist)
        Slist = []; m = m+1;
        for R = Rlist
            for S = find(G(R,:))
                if T(S,D,2) > m
                    Slist = [Slist,S];
                    T(S,D,:) = [R,m,1];
                    pl(S) = plot(x(1,[S,R]),x(2,[S,R]),'r-');
                end
            end
        end
        Rlist = Slist;
    end
    if any(isnan(T(:,D,1)))
        error("The net is not conncted");
        return;
    end
    %drawnow; %pause(0.02);
    for S = 1:N, if(ishandle(pl(S))), delete(pl(S)), end, end
end

diameter = max(max(T(:,:,2)));
fprintf("Net diameter: %f\n", diameter);


%T(:,:,1)



F = randi(N);
plot(x(1,F),x(2,F),'or', 'MarkerSize', 24);
fprintf("Attacker is node %d\n",F);


[CR, C] = count_routes(T, F);
controlled_fraction = (CR / (N-1)^2);% / wG;

[Slist, Dlist] = find(C);
fprintf(1,"Attacker node %d controls %d out of %d routes:\n",F,CR,(N-1)^2)
fprintf(1,"route from %d to %d\n",[Slist';Dlist'])
fprintf("\n");


Ds = [];
Tnew = T;

[~, sort_i] = sort(T(F,:,2));
sort_i = sort_i(2:end);

CRnew = CR;
Cnew = C;
while controlled_fraction < controlled_thresh    
    if isempty(sort_i)
        error('Impossible to reach the threshold');
    end
    D = F;
    while D == F
        % Random D
        %D = randi(N);
        % 2-hop D
        %F_twohops = find(T(F,:, 2) == 2);
        %D = F_twohops(randi(length(F_twohops)));
        % max hops
        D = sort_i(end);
        sort_i = sort_i(1:end-1);
    end
    
    if (isempty(find(Ds == D, 1))) 
    Ds = [Ds; D];
    end
    
    fprintf("Victim is node %d\n",D);
    plot(x(1,D),x(2,D),'og', 'MarkerSize', 24);
    Tnew = inject_routes(G, Tnew, F, D);
    
    [CRnew,Cnew] = count_routes(Tnew,F);
    controlled_fraction = (CRnew / (N-1)^2);% / wG;
    
end


[Slist, Dlist] = find(Cnew);
fprintf(1,"Attacker node %d controls %d out of %d routes:\n",F,CRnew,(N-1)^2)
fprintf(1,"route from %d to %d\n",[Slist';Dlist'])
fprintf("\n");

fprintf("The attacker is now in %d more routes\n", CRnew - CR);

fprintf('Needed %d fake routes\n', length(Ds));


% Color the new routes
Cdiff = abs(Cnew - C);
[Sdiff, Ddiff] = find(Cnew);
for i = 1:length(Sdiff)
    u = Sdiff(i); v = Ddiff(i);
    while u ~= v
        w = Tnew(u, v, 1);
        if (all(Ds ~= w))
            plot(x(1,[u,w]),x(2,[u,w]),'r-');
        end
        u = w;
    end
end
