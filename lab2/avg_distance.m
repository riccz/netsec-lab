close all; clear all;
clc;

N = 50; L =10; Radius = 3;
dV = N/L^2 * pi*Radius^2;

iter = 100;

distances = nan(iter, 1);

iter_i = 1;
while iter_i <= iter
    try
x = L*rand(2,N);

clf;
hold on;

plot(x(1,:),x(2,:),'.b', 'MarkerSize', 12);
text(x(1,:),x(2,:),int2str((1:N)'));

G = zeros(N,N);
%pG = nan(N,N);
disp("Building network graph...");
for i = 1:N
    for j = i+1:N
        if norm(x(:,i)-x(:,j)) < Radius
            G(i,j) = 1; G(j,i) = 1;
            %pG(i,j) = ...
            plot(x(1,[i,j]),x(2,[i,j]),'b-');
            %pG(j,i) = pG(i,j);
            %pause(0.02);
        end
    end
end

degG = sum(sum(G))/N;
wG = degG/(N-1);

fprintf("Nodes per coverage area: %f\n", dV);
fprintf("Average degree: %f\n", degG);
fprintf("Fraction of active links: %f\n", wG);

%conn = yes_or_no("Look at the graph representing the network. Is the graph connected? ");
%if ~conn, disp("Network not connected. Simulation aborted. Re-run a new one."), return, end

T = nan(N,N,3);
pl = nan(N,1);
T(:,:,2) = inf;
T(:,:,3) = 0;

disp("Building routing tables...")
for D = 1:N
    %disp(["for destinaton D = ", int2str(D)])
    m = 0;
    T(D,D,:) = [D,m,1];
    Rlist = [D];
    while ~isempty(Rlist)
        Slist = []; m = m+1;
        for R = Rlist
            for S = find(G(R,:))
                if T(S,D,2) > m
                    Slist = [Slist,S];
                    T(S,D,:) = [R,m,1];
                    pl(S) = plot(x(1,[S,R]),x(2,[S,R]),'r-');
                end
            end
        end
        Rlist = Slist;
    end
    if any(isnan(T(:,D,1)))
        error("The net is not conncted");
        return;
    end
    %drawnow; %pause(0.02);
    for S = 1:N, if(ishandle(pl(S))), delete(pl(S)), end, end
end

diameter = max(max(T(:,:,2)));
fprintf("Net diameter: %f\n", diameter);

distances(iter_i) = mean(mean(T(:,:,2)));

    catch
        disp('Skip not connected');
    end
    iter_i = iter_i+1;
end

m = mean(distances(~isnan(distances)));
fprintf('Avg. distance between a pair of nodes: %f\n', m);
