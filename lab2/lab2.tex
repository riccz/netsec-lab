\documentclass[a4paper,11pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage[margin=2.54cm]{geometry}

\usepackage{codecmds}

%\usepackage{amsmath}
%\usepackage{amssymb}
\usepackage{siunitx}

%\usepackage{tikz}
%\usetikzlibrary{chains,scopes,arrows.meta}

%\usepackage{import}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage{color}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage[section]{placeins}

%\usepackage[backend=biber]{biblatex}
%\addbibresource{}

\author{Riccardo Zanol}
\date{May 15, 2017}
\title{Wireless Network Security \\ Lab. 2}
%\title{Secure routing in ad hoc wireless networks: attacks on routing}

\begin{document}
\matlabcodeconfig
\maketitle

The first four steps of the instructions generate a random network of
$N=50$ nodes placed in a square of size $L=\SI{10}{\km}$ with a
coverage radius of $R=\SI{3}{\km}$. Then they build the routing table
for each pair of source and destination $(S,D)$ to simulate a
DSDV-like routing protocol. Finally a random attacker node, $F$, is
selected and the number of routes that pass through it are counted.

The fifth step requires a randomly chosen malicious node $F$ to inject
fake routing messages. This is simulated by selecting a node $D$,
which will be the destination carried by the forged routes, and
replacing the routing entry $T(F,D)$ with $[D,0,1]$. This means that
node $F$ claims to have a route of cost 0 toward node $D$, therefore
the neighbors of $F$ will react to this forged route, according to the
protocol, by updating their routing table if the route offered by $F$
has a smaller path cost. This process then proceeds recursively like
the route-population algorithm and is implemented as shown in
Listing~\ref{lst:inject_routes}.  After the forged route has
propagated through the network, the routes that pass through $F$ are
counted again.
%
\includecode{inject_routes}

By running some simulations with randomly chosen $F$ and $D$ it is
possible to notice that the attacker is able to gain control over more
routes when it picks a $D$ that is as far as possible from it. If this
happens the forged routing messages are propagated farther away from
$F$, since the first nodes that do not react to them are those at
equal distance from $D$ and $F$, which are not able to improve their
path cost toward $D$ by using the fake route.
%
An example of this is shown in Figure~\ref{fig:routes}. Here the
attacker chooses a node $D$ that is on the opposite side of the
network, so it can induce the nodes in its own half of the network to
send packets to $D$ through it. Before the attack it controls 82
routes and the injected messages allow it to control 29 additional
routes.
%
\begin{figure}
  \centering
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{true_routes}
    \caption{}
    \label{fig:routes_before}
  \end{subfigure}%
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{true_mask}
    \caption{}
    \label{fig:mask_before}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
      \centering
      \includegraphics[width=\textwidth]{fake_routes}
      \caption{}
      \label{fig:routes_after}
  \end{subfigure}%
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{fake_mask}
    \caption{}
    \label{fig:mask_after}
  \end{subfigure}
  \caption{Example of effect of a forged route from node $F=41$ with a
    destination at maximum distance
    $D=10$. (\subref{fig:routes_before}) and
    (\subref{fig:mask_before}) show the situation when $F$ behaves
    honestly.  (\subref{fig:routes_after}) and
    (\subref{fig:mask_after}) show the controlled routes after the
    injection of the forged route.}
  \label{fig:routes}
\end{figure}

Since the computation of the averages in the following paragraphs
requires many simulations to be run, it is faster to check
automatically whether the network is connected, instead of waiting for
a manual confirmation at each run. To this end the algorithm that
populates the routing table $T$ is modified to exit the loop when
\inlinecode{Rlist} is empty and then check whether there are any nodes
without a next hop.

The simulations were done by generating $1000$ random network
topologies with the same $N$, $L$, $R$ and, for each of them,
selecting a random attacker $F$ and a random destination $D$ for the
forged routes according to three different strategies.
%
First the choice of $D$ is made at random among all nodes, then it is
picked among the nodes at distance 2 from $F$ and, finally, it is
chosen between the nodes at maximum distance from $F$.
%
The results are shown in Table~\ref{tab:avg}.
%
\begin{table}
  \centering
  \begin{tabular}{lccc}
    & Random & 2-hop distance & Max distance \\ \hline
    Honest $F$ & 77.05 & 77.05 & 77.05 \\
    Dishonest $F$ & 93.13 & 91.50 & 103.86 \\
    Increase & 16.09 & 14.46 & 26.81
  \end{tabular}
  \caption{Comparison between the average number of routes controlled
    by $F$ after the injection of a forged route. A route $(S,D)$ is
    considered controlled when the path in the routing table from $S$
    to $D$ passes through $F$ at some point.}
  \label{tab:avg}
\end{table}
%
As can be seen, the choice of a $D$ at maximum distance from $F$
allows the attacker to gain control over more routes. The other two
choices are close to each other, but this is because the average
distance between any two nodes is \SI{2.5}{{hops}}, hence the two
strategies are not much different on average.

To see how many routes to different destinations an attacker needs to
forge in order to control a certain fraction of the routes, the same
attackers in the same 1000 topologies started with the previous choice
of $D$, made using the maximum-distance selection strategy, and sent
out forged routing messages for all the remaining nodes, $D_2, D_3,
\dots, D_{N-1}$ one after the other and ordered by distance from $F$,
in order to take advantage of the fact seen in the previous paragraph.
%
The number of controlled routes is counted after each new forged
message and their average fraction is plotted in
Figure~\ref{fig:avg_cr}. Figure~\ref{fig:avg_succ} shows the
probability that an attacker can control the given fraction of routes
when forging $N-1$ routing messages. Finally in Table~\ref{tab:thresh}
there are the success probabilities and average number of nodes
required to meet the three given thresholds.

The results show that, while the threshold is low, it can still be
met, but for higher thresholds it becomes very hard to control enough
routes from a single attacker $F$ unless the network topology is very
favorable. Even in those cases where it is possible, node $F$ must
send forged routes for the vast majority of the other nodes to gain
control over a significant fraction of the routes.
%
\begin{figure}
  \centering
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{avg_controlled}
    \caption{Average fraction of controlled routes with a given number
      of messages.}
    \label{fig:avg_cr}
  \end{subfigure}%
  \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{avg_success}
    \caption{Success probability of gaining control over a given
      fraction of controlled routes.}
    \label{fig:avg_succ}
  \end{subfigure}
  \caption{Controlled routes when multiple messages are injected.}
\end{figure}
%
\begin{table}
  \centering
  \begin{tabular}{ccc}
    Threshold & Success probability & Number of destinations \\ \hline
    \SI{20}{\percent} & 0.932 & 19.78 \\
    \SI{50}{\percent} & 0.122 & 40.25 \\
    \SI{90}{\percent} & 0 & -
  \end{tabular}
  \caption{Average number of injected routes for different
    destinations required to control a given percentage of the total
    routes.}
  \label{tab:thresh}
\end{table}

A more effective way of gaining control over many routes could be to
send also an incremented sequence number, in this way even nodes which
have a better path to $D$ would prefer the newer route through
$F$. The effectiveness depends, however, on what will node $D$ do when
it receives a route for itself with a version number that it never
sent out: it could increment the sequence number again, thus leading
to an oscillating routing table where $D$ and $F$ alternate as the
destination of the packets addressed to $D$.

\end{document}
