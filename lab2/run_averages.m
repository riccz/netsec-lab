close all; clear all; clc;
delete('matlab.log');
diary('matlab.log');

rng('shuffle');

N = 50; L = 10; R = 3;
iter = 1000;
thresholds = [0.2, 0.5, 0.8];
methods = {'random', '2hops', 'maxdist'};

CR_vec = nan(iter, 1);
CRnew_one_vec = nan(iter, 3);
CRnew_many_vec = nan(iter, N-1);

parfor i = 1:iter
    np = setup_network(N,L,R);
    F = choose_F(N, np.x);
    
    [CR_vec(i), ~] = count_routes(np.T, F);
    
    CRone = nan(3, 1);
    for m = 1:length(methods)
        D = choose_D(np.x, np.T, F, methods{m}, []);
        Tnew = inject_routes(np.G, np.T, F, D);
        [CRone(m), ~] = count_routes(Tnew, F);
    end
    CRnew_one_vec(i,:) = CRone;
    
    CRmany = nan(N-1, 1);
    CRmany(1) = CRone(3);
    Ds = [D];
    for d = 2:N-1
        D = choose_D(np.x, Tnew, F, 'maxdist', Ds);
        Ds = [Ds D];
        Tnew = inject_routes(np.G, Tnew, F, D);
        [CRmany(d), ~] = count_routes(Tnew, F);
    end
    CRnew_many_vec(i,:) = CRmany;
end

close all;

avg_honest = mean(CR_vec);
avg_one = mean(CRnew_one_vec, 1);
avg_gain = avg_one - avg_honest;

CR_fraction = CRnew_many_vec ./ (N-1)^2;
avg_cr_fraction = mean(CR_fraction, 1);

succ_prob = @(t) sum(any(CR_fraction >= t, 2)) / iter;

fprintf('Average controlled routes, honest F: %f\n', avg_honest);

for m = 1:length(methods)
    fprintf('Avg. controlled routes, one forging D %s: %f, gain: %f\n', ...
        methods{m}, avg_one(m), avg_gain(m));
end

for t = 1:length(thresholds)
    succ_p = succ_prob(thresholds(t));
    first_succ = [];
    for i = 1:iter
        f = find(CR_fraction(i,:) >= thresholds(t), 1);
        if ~isempty(f)
            first_succ = [first_succ f];
        end
    end
    needed_D = mean(first_succ);
    
    fprintf('Threshold %f:\n', thresholds(t));
    fprintf('Can reach the threshold wp %f\n', succ_p);
    fprintf('Avg. needed nodes: %f\n', needed_D);
    fprintf('\n');
end

figure;
plot(1:N-1, avg_cr_fraction);
xlabel('Forged routes');
ylabel('Controlled fraction');
grid on;
print('avg_controlled.eps', '-depsc');

thr = linspace(0, 1, 1024);
for i = 1:length(thr)
    probs(i) = succ_prob(thr(i));
end
figure;
plot(thr, probs);
xlabel('Controlled fraction');
ylabel('Success probability');
grid on;
print('avg_success.eps', '-depsc');

diary('off');
