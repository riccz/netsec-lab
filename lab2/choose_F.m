function F = choose_F(N, x)
F = randi(N);
fprintf('Attacker is node %d\n',F)
plot(x(1,F),x(2,F),'ro','MarkerSize',20,'LineWidth',5);
end
