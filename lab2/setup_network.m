function netparams = setup_network(N, L, R)
while true
    try
        netparams = maybe_setup_network(N, L, R);
        return;
    catch
        % loop on non connected graph
    end
end
end

function netparams = maybe_setup_network(N, L, R)
figure(1)
density = N/L^2*pi*R^2
x = L*rand(2,N);
clf, hold on
pV = plot(x(1,:),x(2,:),'.');
text(x(1,:),x(2,:),int2str((1:N)'),'FontSize',16);
G = zeros(N,N); pG = nan(N,N);
disp('Building network graph...')
for i = 1:N;
    for j = i+1:N;
        y = [];
        if norm(x(:,i)-x(:,j)) < R,
            G(i,j) = 1; G(j,i) = 1;
            pG(i,j) = plot(x(1,[i,j]),x(2,[i,j]),'b-'); %pause(0.02);
            axis equal
        end
    end
end
degree = sum(sum(G))/N
edge_fraction = degree/(N-1)

% conn = input('Look at the graph representing the network. Is it connected? Y/N [Y]:','s');
% if isempty(conn)
%     conn = 'Y';
% end
% if lower(conn(1)) == 'n'
%     disp('Network not connected. Simulation aborted. Re-run a new one.')
%     return
% end

T = nan(N,N,3);
T(:,:,2) = inf;
T(:,:,3) = 0;
pT = nan(N,N);
disp('Building routing tables...')
for D = 1:N
    %disp(['for destinaton D = ', int2str(D)]);
    m = 0;
    T(D,D,:) = [D,m,1];
    pT(D,D) = plot(x(1,D),x(2,D),'go','MarkerSize',20,'LineWidth',5);
    Rlist = [D];
    while ~isempty(Rlist)
        Slist = []; m = m+1;
        for R = Rlist';
            for S = find(G(R,:));
                if T(S,D,2) > m;
                    Slist = [Slist; S];
                    T(S,D,:) = [R,m,1];
                    pT(S,R) = plot(x(1,[S,R]),x(2,[S,R]),'g-','LineWidth',4);
                end
            end
        end
        Rlist = Slist;
    end
    if any(isnan(T(:,D,1)))
        error('The net is not conncted');
    end
    %drawnow;
    %pause(0.02)
    pTdel = pT(~isnan(pT));
    if ~isempty(pTdel)
        delete(pTdel); pT = nan(N,N);
    end
end
diameter = max(max(T(:,:,2)))
%T(:,:,1)

netparams.x = x;
netparams.G = G;
netparams.T = T;
netparams.diameter = diameter;
netparams.degree = degree;
netparams.edge_fraction = edge_fraction;
netparams.density = density;
end
